function isPrime(num) {
  if (num < 2) return false;
  for (var i = 2; i < num; i++) {
    if (num % i == 0) return false;
  }
  return true;
}

function inKQ() {
  const n = document.getElementById("n-number").value * 1;
  let ketqua = 0;
  for (var i = 0; i < n; i++) {
    if (isPrime(i)) {
      ketqua = ketqua + i + ` `;
      document.getElementById(`result`).innerHTML = ketqua;
    }
  }
}
