function tinhGiaiThua() {
  const n = document.getElementById("n-number").value * 1;
  let tich = 1;
  if (n == 0) {
    document.getElementById(`result`).innerHTML = `Không thể tính giai thừa`;
  } else {
    for (i = 1; i <= n; i++) {
      tich = tich * i;
      document.getElementById(`result`).innerHTML = `Giai thừa: ${tich}`;
    }
  }
}
